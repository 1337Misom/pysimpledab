import pysimpledab as dab
from time import sleep
from datetime import datetime
import ctypes
import pyaudio
import array
import struct
from ringbuf import RingBuffer
signal = False
ring_buf = RingBuffer(format='<h', capacity=38400*2)
program_names = []

timeout = 3

p = pyaudio.PyAudio()
dab_class = dab.SimpleDAB()

def play_callback(in_data,frame_count,time_info,status):
    data = bytes(ring_buf.pop(frame_count*2))
    return (data, pyaudio.paContinue)

def programname_callback(name, length, data):
    name_decoded = name.decode("utf-8")

    if name_decoded not in program_names:
        program_names.append(name_decoded)
    return None

def audioOut_callback(buffer, size, samplerate, stereo, data):
    py_buf = ctypes.cast(buffer,ctypes.POINTER(ctypes.c_short * size)).contents
    ring_buf.push(py_buf)
    return None

def dataOut_callback(label, ctx):
    #print(label)
    return None

def bytesOut_callback(data, amount, k, ctx):
    return None

def motdata_callback(data, size,name,d, ctx):
    file = open(name.decode("utf-8"), "wb")
    py_buf = bytes(ctypes.cast(data,ctypes.POINTER(ctypes.c_ubyte*size)).contents)
    file.write(py_buf)
    file.close()
    return None

def syncsignal_callback(flag, data):
    global signal
    if flag:
        signal = True
    return None

def ensemble_callback(name,length,user_data):
    #print(name,length,user_data)
    return None

api_struct = dab.Callbacks()


api_struct.dabMode = 1
api_struct.syncsignal_Handler = dab.syncsignal_t(syncsignal_callback)
api_struct.systemdata_Handler = dab_class.dummy_system_callback
api_struct.ensemblename_Handler = dab.ensemblename_t(ensemble_callback)
api_struct.programname_Handler = dab.programname_t(programname_callback)
api_struct.fib_quality_Handler = dab_class.dummy_fib_callback
api_struct.audioOut_Handler = dab.audioOut_t(audioOut_callback)
api_struct.dataOut_Handler = dab.dataOut_t(dataOut_callback)
api_struct.bytesOut_Handler = dab_class.dummy_bytes_callback
api_struct.programdata_Handler = dab_class.dummy_program_data_callback
api_struct.program_quality_Handler = dab_class.dummy_program_qual_callback
api_struct.motdata_Handler = dab.motdata_t(motdata_callback)
api_struct.tii_data_Handler = dab_class.dummy_tii_callback
api_struct.timeHandler = dab_class.dummy_time_callback

dab_class.begin(dab.RTLSDR,api_struct)
dab_class.set_gain(100)
dab_class.start(dab.BAND_III,"5A")

while True:
    program_names = []
    channel = input("Enter Channel : ")
    dab_class.switch_frequency(dab.BAND_III,channel)
    start_time = datetime.now()
    while not signal:
        if (datetime.now() - start_time).seconds >= timeout:
            dab_class.exit()
            print("No signal found")
            exit(1)

    sleep(5)

    for x,i in enumerate(program_names):
        print(x,")",i)

    inp = int(input("Enter Station : "))

    station_name = program_names[inp]
    if not dab_class.is_audio(station_name.encode("ascii")):
        print("program not audio")
        break
    dab_class.switch_station(station_name)
    sleep(5)
    stream = p.open(format=pyaudio.paInt16,channels=2,rate=48000,output=True,stream_callback=play_callback)
    resp = input("New selection? (y/n) : ")
    if resp == "n":
        break
    stream.close()

dab_class.exit()
p.terminate()
