import pysimpledab as dab
from time import sleep
from datetime import datetime
import ctypes

signal = False
program_names = []

timeout = 2
current_channel = "5A"
def programname_callback(name, length, data):
    name_decoded = name.decode("utf-8")
    if name_decoded not in program_names:
        program_names.append((current_channel,name_decoded))
    #print(name_decoded)
    return None

def audioOut_callback(buffer, size, samplerate, stereo, data):
    return None

def dataOut_callback(label, ctx):
    return None

def bytesOut_callback(data, amount, k, ctx):
    return None

def motdata_callback(data, size,name,d, ctx):
    return None

def syncsignal_callback(flag, data):
    global signal
    if flag:
        signal = True
    return None

dab_class = dab.SimpleDAB()

api_struct = dab.Callbacks()
api_struct.dabMode = 1
api_struct.syncsignal_Handler = dab.syncsignal_t(syncsignal_callback)
api_struct.systemdata_Handler = dab_class.dummy_system_callback
api_struct.ensemblename_Handler = dab_class.dummy_ensemble_callback
api_struct.programname_Handler = dab.programname_t(programname_callback)
api_struct.fib_quality_Handler = dab_class.dummy_fib_callback
api_struct.audioOut_Handler = dab.audioOut_t(audioOut_callback)
api_struct.dataOut_Handler = dab.dataOut_t(dataOut_callback)
api_struct.bytesOut_Handler = dab_class.dummy_bytes_callback
api_struct.programdata_Handler = dab_class.dummy_program_data_callback
api_struct.program_quality_Handler = dab_class.dummy_program_qual_callback
api_struct.motdata_Handler = dab.motdata_t(motdata_callback)
api_struct.tii_data_Handler = dab_class.dummy_tii_callback
api_struct.timeHandler = dab_class.dummy_time_callback


dab_class.begin(dab.RTLSDR,api_struct)
dab_class.set_gain(50)


dab_class.start(dab.BAND_III,"5A")
sleep(5)

for channel in dab.BAND_III_CHANNELS:
    current_channel = channel
    start_time = datetime.now()
    dab_class.switch_frequency(dab.BAND_III,channel)
    signal = False
    while not signal:
        if (datetime.now() - start_time).seconds >= timeout:
            print(channel,": No signal found")
            break
    if signal == False:
        continue
    sleep(5)

for channel in dab.L_BAND_CHANNELS:
    current_channel = channel
    start_time = datetime.now()
    dab_class.switch_frequency(dab.BAND_III,channel)
    signal = False
    while not signal:
        if (datetime.now() - start_time).seconds >= timeout:
            print(channel,": No signal found")
            break
    if signal == False:
        continue
    sleep(5)

print(program_names)
sleep(1)
input("Exit? : ")
dab_class.exit()
